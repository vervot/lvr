#! /usr/bin/env python3
import os
from pathlib import Path
import unittest
import sys
from contextlib import contextmanager
from io import StringIO

import lvr_db
import queries.query
import expected as exp

#TODO: load from multiple lvr.csv

@contextmanager
def captured_output():
    new_out, new_err = StringIO(), StringIO()
    old_out, old_err = sys.stdout, sys.stderr
    try:
        sys.stdout, sys.stderr = new_out, new_err
        yield sys.stdout, sys.stderr
    finally:
        sys.stdout, sys.stderr = old_out, old_err

class TestLvrDbClass(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.maxDiff = None # too see full values in DIFF on assert failure
        cls.herePath = os.path.realpath(__file__) # ~/sandbox/lvr/lvr/tests.py
        cls.datadir = str(Path(cls.herePath).parent.parent  / 'test-data')
        cls.outdir  = str(Path(cls.herePath).parent.parent  / 'OUT-FILES')
        os.makedirs(cls.datadir, exist_ok=True)
        os.makedirs(cls.outdir, exist_ok=True)
        cls.dbfile = str(Path(cls.outdir) / 'UNIT-TEST.db')
        cls.lvrlist = [str(Path(cls.datadir) / 'lvr-test.csv'),
                       str(Path(cls.datadir) / 'lvr-test1.csv'),
                       str(Path(cls.datadir) / 'lvr-test2.csv'), ]
    def cleanpaths(self, outstr):
        """Remove installation specific top dirs from output"""
        return outstr.replace(self.datadir,'').replace(self.outdir,'').strip()
    
    def setUp(self):
        self.lvrdb = lvr_db.LvrDb(self.dbfile)
        with captured_output() as (out, err):
            self.lvrdb.insert_LVR_from_csv_files(self.lvrlist)
        
    def test_insert_LVR_from_csv_files(self):
        """Load one file"""
        lvrdb0 = lvr_db.LvrDb(self.dbfile)
        with captured_output() as (out, err):
            totalballots = lvrdb0.insert_LVR_from_csv_files(self.lvrlist)
        output = self.cleanpaths(out.getvalue().strip())
        if verbosity==2:
            print('DBG-insert output="{}"'.format(output))
        self.assertEqual(output, exp.insert, 'Actual to Expected')
        self.assertEqual(totalballots, 54, 'Actual to Expected')

    def test_summary(self):
        with captured_output() as (out, err):
            self.lvrdb.summary()
        output = self.cleanpaths(out.getvalue().strip())
        if verbosity==2:
            print('DBG-summary output="{}"'.format(output))
        self.assertEqual(output, exp.summary, 'Actual to Expected')

    def test_count_totals(self):
        csvfile = StringIO()
        queries.query.count_totals(self.dbfile, csvfile=csvfile)
        actual = self.cleanpaths(csvfile.getvalue().strip()).replace('\r','')
        if verbosity==2:
            print('DBG-count_totals actual="{}"'.format(actual))
        self.assertEqual(actual,exp.count_totals, 'Actual to Expected')

    def test_count_by_precinct(self):
        csvfile = StringIO()
        queries.query.count_by_precinct(self.dbfile, csvfile=csvfile)
        actual = self.cleanpaths(csvfile.getvalue().strip()).replace('\r','')
        if verbosity==2:
            print('DBG-count_by_precinct actual="{}"'.format(actual))
        self.assertEqual(actual,exp.count_by_precinct, 'Actual to Expected')

    def test_count_precinct(self):
        csvfile = StringIO()
        precinct='179'
        queries.query.count_precinct(self.dbfile, precinct, csvfile=csvfile)
        actual = self.cleanpaths(csvfile.getvalue().strip()).replace('\r','')
        if verbosity==2:
            print('DBG-count_precinct actual="{}"'.format(actual))
        self.assertEqual(actual,exp.count_precinct, 'Actual to Expected')

    def test_vote_correlation(self):
        csvfile = StringIO()
        (race1, choice1) = ('DOGCATCHER','RED, RHODA')
        (race2, choice2) = ('PROPOSITION 4','YES')
                            
        with captured_output() as (out, err):
            queries.query.vote_correlation(self.dbfile,
                                           race1,choice1, race2,choice2)
        output = self.cleanpaths(out.getvalue().strip())
        if verbosity==2:
            print('DBG-vote_correlation output="{}"'.format(output))
        self.assertEqual(output,exp.vote_correlation, 'Actual to Expected')

    @unittest.skip("Implementation not complete")
    def test_count_ineffective_ballots(self):
        "Count Ballots with no effective votes"
        csvfile = StringIO()
        queries.query.ineffective_ballots(self.dbfile, csvfile=csvfile)
        actual = self.cleanpaths(csvfile.getvalue().strip())
        if verbosity==2:
            print('DBG-count_ineffective_ballots actual="{}"'.format(actual))
        self.assertEqual(actual,exp.ineffective_ballots, 'Actual to Expected')
        
        
if __name__ == '__main__':
    #verbosity=2
    verbosity=1
    unittest.main(verbosity=verbosity)
        
