#! /usr/bin/env python3

import os, sys, string, argparse, logging
import query

def main():
    parser = argparse.ArgumentParser(
        description=('Do stuff'),
        epilog='EXAMPLE: %(prog)s a b"'
        )
    parser.add_argument('dbfile', help='SQlite database holding votes (LVR)')

    parser.add_argument('--csvfile', help='Output csvfile',
                        type=argparse.FileType('w') )
    parser.add_argument('--htmlfile', help='Output htmlfile',
                        type=argparse.FileType('w') )

    parser.add_argument('--loglevel',      help='Kind of diagnostic output',
                        choices = ['CRTICAL','ERROR','WARNING','INFO','DEBUG'],
                        default='WARNING',
                        )
    args = parser.parse_args()

    log_level = getattr(logging, args.loglevel.upper(), None)
    if not isinstance(log_level, int):
        parser.error('Invalid log level: %s' % args.loglevel) 
    logging.basicConfig(level = log_level,
                        format='%(levelname)s %(message)s',
                        datefmt='%m-%d %H:%M'
                        )
    logging.debug('Debug output is enabled!!!')

    query.count_totals(args.dbfile, 
                       csvfile=args.csvfile, htmlfile=args.htmlfile)


if __name__ == '__main__':
    main()
