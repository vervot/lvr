"""Query Library"""
import sqlite3
import csv
import logging
import pdb

def fetchsingle(con, sql, sql_args):
    """Get the first value from the first row of a resultset"""
    c = con.cursor()
    c.execute(sql, sql_args)
    row1 = c.fetchone()
    #!assert row1, 'Query got no results'
    if not row1:
        msg = ('Query got no results. sql="{}", sql_args={}'.
               format(sql, sql_args))
        raise Exception(msg)
    return row1[0]

def vote_correlation(dbfile, race1,choice1, race2,choice2):
    con = sqlite3.connect(dbfile)
    
    qc=('SELECT choice.choice_id FROM race,choice' 
        ' WHERE race.race_id = choice.race_id'
        ' AND race.title=? AND choice.title=?')
    cid1 = fetchsingle(con, qc, (race1, choice1))
    cid2 = fetchsingle(con, qc, (race2, choice2))
    rid2 = fetchsingle(con,'SELECT race_id FROM race WHERE race.title = ?',
                       (race2,))
    
    qcovote=('SELECT count(v1.cvr_id) FROM vote as v1'
             ' INNER JOIN vote as v2'
             ' ON v1.choice_id=?'
             ' AND v2.choice_id=? AND v1.cvr_id=v2.cvr_id')
    covote = fetchsingle(con,qcovote, (cid1, cid2))

    qvote1=('SELECT count(v1.cvr_id) FROM vote as v1 WHERE v1.choice_id=? '
            ' AND EXISTS (SELECT * FROM choice,vote '
            ' WHERE choice.race_id=? AND choice.choice_id = vote.choice_id '
            ' AND vote.cvr_id = v1.cvr_id)')
    vote1 = fetchsingle(con, qvote1, (cid1,rid2))
    
    msg = 'percentage co-vote = {:.0%}'.format(covote / vote1)
    print(msg)


HTMLSTYLE='''
  <style>
  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }
  
  td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
  }
  
  tr:nth-child(even) {
    background-color: #dddddd;
  }
  </style>
'''

HTMLHEAD='''
<!DOCTYPE html>
<html lang=\"en\">
<head>
  <title>{title}</title>
</head>
{style}
<body>
  <h1>{title}</h1>
<table>
'''

HTMLFOOT='''
</table>
</body>
</html>
'''


def sql2file(dbfile, title, sql, csvfile=None, htmlfile=None):
    logging.debug(title)
    con = sqlite3.connect(dbfile)
    c = con.cursor()

    if htmlfile:
        print(HTMLHEAD.format(style=HTMLSTYLE,title=title), file=htmlfile)

    if csvfile:
        csvwriter = csv.writer(csvfile)

    c.execute(sql)
    rowcnt = 0
    for row in c.fetchall():
        logging.debug('Row={}'.format(row))
        rowcnt += 1
        if csvfile:
            csvwriter.writerow(row)
        if htmlfile:
            #pdb.set_trace()
            html ='<TR>'
            html += ''.join(['<TD>'+str(v)+'</TD>' for v in row])
            html += '</TR>'
            print(html, file=htmlfile)
    if htmlfile:
        print(HTMLFOOT, file=htmlfile)
    return(rowcnt)

def count_totals(dbfile, csvfile=None, htmlfile=None):
    title = 'COUNT of Total votes for ALL Choices, ALL Races'
    SQL=('SELECT count(vote.cvr_id) as Votes, '
         'race.title as Race, choice.title as Choice '
         'FROM vote,choice,race '
         'WHERE vote.choice_id = choice.choice_id '
         'AND choice.race_id = race.race_id '
         'GROUP BY race.column, choice.choice_id')
    return sql2file(dbfile, title, SQL, csvfile=csvfile, htmlfile=htmlfile)

    
def count_by_precinct(dbfile, csvfile=None, htmlfile=None):
    title="COUNT of Total votes for All Choices, All Races, by Precinct"    
    SQL=('SELECT count(vote.cvr_id) as Votes, '
         'cvr.precinct as Precinct, '
         'choice.title as Choice, '
         'race.title as Race '
         'FROM vote,choice,race,cvr '
         'WHERE vote.choice_id = choice.choice_id '
         'AND choice.race_id = race.race_id '
         'AND vote.cvr_id = cvr.cvr_id '
         'GROUP BY  cvr.precinct, race.column, choice.choice_id')
    return sql2file(dbfile, title, SQL, csvfile=csvfile, htmlfile=htmlfile)

def count_precinct(dbfile, precinct, csvfile=None, htmlfile=None):
    title=('COUNT of Total votes for All Choices, All Races, Precinct={}'
           .format(precinct))

    SQL=('SELECT count(vote.cvr_id) as Votes, '
         'cvr.precinct as Precinct, '
         'choice.title as Choice, '
         'race.title as Race '
         'FROM vote,choice,race,cvr '
         'WHERE vote.choice_id = choice.choice_id '
         'AND choice.race_id = race.race_id '
         'AND vote.cvr_id = cvr.cvr_id '
         'AND cvr.precinct = \'{}\' '
         'GROUP BY  cvr.precinct, race.column, choice.choice_id')
    return sql2file(dbfile, title, SQL.format(precinct),
                    csvfile=csvfile, htmlfile=htmlfile)


def ineffective_ballots(dbfile, csvfile=None, htmlfile=None):
    """LVR row ('ballot') where all choices are 'undervote' or 'overvote'
    are effectively a non-voting ballot."""

    title='COUNT of ballots with only undervote or overvotes'
    SQL=("SELECT DISTINCT cvr.cvr_id FROM cvr,vote,choice "
         "WHERE cvr.cvr_id = vote.cvr_id "
         "AND vote.choice_id = choice.choice_id "
         "AND (choice.title = 'undervote' OR choice.title = 'overvote')")
    return sql2file(dbfile, title, SQL, csvfile=csvfile, htmlfile=htmlfile)
    

