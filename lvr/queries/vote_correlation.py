#! /usr/bin/env python3
'''\
Correlate votes from one Race/Choice combo to another Race/Choice combo.
'''

import os, sys, string, argparse, logging

import query

def lvr_query(dbfile, race1,choice1, race2,choice2):
    return query.vote_correlation(dbfile, race1,choice1, race2,choice2)
    

##############################################################################

def main_tt():
    cmd = 'MyProgram.py foo1 foo2'
    sys.argv = cmd.split()
    res = main()
    return res

def main():
    #!print('EXECUTING: {}\n\n'.format(' '.join(sys.argv)))
    parser = argparse.ArgumentParser(
        #!version='1.0.1',
        description=('Correlate votes from one Race/Choice combo to'
                     ' another Race/Choice combo.'),
        epilog='EXAMPLE: %(prog)s a b"'
        )
    parser.add_argument('dbfile',  help='SQlite database file containing votes.')
    parser.add_argument('race1',  help='Title of Race 1')
    parser.add_argument('choice1',  help='Title of Choice 1')
    parser.add_argument('race2',  help='Title of Race 2')
    parser.add_argument('choice2',  help='Title of Choice 2')



    parser.add_argument('--loglevel',      help='Kind of diagnostic output',
                        choices = ['CRTICAL','ERROR','WARNING','INFO','DEBUG'],
                        default='WARNING',
                        )
    args = parser.parse_args()
    #!args.outfile.close()
    #!args.outfile = args.outfile.name

    #!print 'My args=',args
    #!print 'infile=',args.infile


    log_level = getattr(logging, args.loglevel.upper(), None)
    if not isinstance(log_level, int):
        parser.error('Invalid log level: %s' % args.loglevel) 
    logging.basicConfig(level = log_level,
                        format='%(levelname)s %(message)s',
                        datefmt='%m-%d %H:%M'
                        )
    logging.debug('Debug output is enabled!!!')


    query.vote_correlation(args.dbfile,
                           args.race1, args.choice1,
                           args.race2,args.choice2)

if __name__ == '__main__':
    main()
