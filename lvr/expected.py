insert = '''Removed LVR database: /UNIT-TEST.db
Created schema in LVR database: /UNIT-TEST.db
-----------------
Adding CSV (/lvr-test.csv) content to LVR database /UNIT-TEST.db
25 total ballots loaded so far
25 ballots loaded for file "/lvr-test.csv"
-----------------
Adding CSV (/lvr-test1.csv) content to LVR database /UNIT-TEST.db
29 total ballots loaded so far
4 ballots loaded for file "/lvr-test1.csv"
-----------------
Adding CSV (/lvr-test2.csv) content to LVR database /UNIT-TEST.db
-----------------
54 total ballots loaded
25 ballots loaded for file "/lvr-test2.csv"
=================
Creating indexes......done
-----------------
Gaps in CVR Numbers found:
   Between 40 and 112 (length: 71)'''

summary = '''Summarize database: /UNIT-TEST.db

LVR Database Summary:
   Sources: /lvr-test.csv, /lvr-test1.csv, /lvr-test2.csv 
   Race count: 2
   Count (VoteFor,Choices) per race: (1, 6),(1, 9)
###################################################################'''

count_totals = '''14,DOGCATCHER,"RED, RHODA"
26,DOGCATCHER,"BLUE, BOBBY"
4,DOGCATCHER,undervote
4,DOGCATCHER,overvote
2,DOGCATCHER,"GREEN, GRETA"
2,DOGCATCHER,Write-in
20,PROPOSITION 4,NO
15,PROPOSITION 4,YES
19,PROPOSITION 4,undervote
5,PROPOSITION 4,overvote
11,PROPOSITION 4,"YELLOW, YAZ"
12,PROPOSITION 4,"ORANGE, OLLIE"
7,PROPOSITION 4,"PURPLE, PAUL"
8,PROPOSITION 4,"PINK, PENNY"
2,PROPOSITION 4,Write-in'''

count_by_precinct = '''2,37,"RED, RHODA",DOGCATCHER
14,37,"BLUE, BOBBY",DOGCATCHER
2,37,undervote,DOGCATCHER
3,37,NO,PROPOSITION 4
8,37,YES,PROPOSITION 4
8,37,undervote,PROPOSITION 4
1,37,overvote,PROPOSITION 4
4,37,"YELLOW, YAZ",PROPOSITION 4
5,37,"ORANGE, OLLIE",PROPOSITION 4
2,37,"PURPLE, PAUL",PROPOSITION 4
3,37,"PINK, PENNY",PROPOSITION 4
1,37,Write-in,PROPOSITION 4
4,40,"RED, RHODA",DOGCATCHER
2,40,"BLUE, BOBBY",DOGCATCHER
2,40,undervote,DOGCATCHER
2,40,overvote,DOGCATCHER
2,40,NO,PROPOSITION 4
1,40,YES,PROPOSITION 4
3,40,undervote,PROPOSITION 4
2,40,overvote,PROPOSITION 4
1,40,"YELLOW, YAZ",PROPOSITION 4
2,40,"ORANGE, OLLIE",PROPOSITION 4
2,40,"PURPLE, PAUL",PROPOSITION 4
6,99,"BLUE, BOBBY",DOGCATCHER
2,99,overvote,DOGCATCHER
2,99,"GREEN, GRETA",DOGCATCHER
2,99,Write-in,DOGCATCHER
6,99,NO,PROPOSITION 4
4,99,YES,PROPOSITION 4
4,99,undervote,PROPOSITION 4
2,99,overvote,PROPOSITION 4
3,99,"YELLOW, YAZ",PROPOSITION 4
3,99,"ORANGE, OLLIE",PROPOSITION 4
1,99,"PURPLE, PAUL",PROPOSITION 4
1,99,"PINK, PENNY",PROPOSITION 4
4,162,"RED, RHODA",DOGCATCHER
4,162,"BLUE, BOBBY",DOGCATCHER
7,162,NO,PROPOSITION 4
3,162,undervote,PROPOSITION 4
2,162,"YELLOW, YAZ",PROPOSITION 4
2,162,"ORANGE, OLLIE",PROPOSITION 4
1,162,"PURPLE, PAUL",PROPOSITION 4
4,162,"PINK, PENNY",PROPOSITION 4
4,179,"RED, RHODA",DOGCATCHER
2,179,NO,PROPOSITION 4
2,179,YES,PROPOSITION 4
1,179,undervote,PROPOSITION 4
1,179,"YELLOW, YAZ",PROPOSITION 4
1,179,"PURPLE, PAUL",PROPOSITION 4
1,179,Write-in,PROPOSITION 4'''


count_precinct = '''4,179,"RED, RHODA",DOGCATCHER
2,179,NO,PROPOSITION 4
2,179,YES,PROPOSITION 4
1,179,undervote,PROPOSITION 4
1,179,"YELLOW, YAZ",PROPOSITION 4
1,179,"PURPLE, PAUL",PROPOSITION 4
1,179,Write-in,PROPOSITION 4'''


#vote_correlation = '''percentage co-vote = 29%'''
vote_correlation = '''percentage co-vote = 23%''' # exclude "blanks"

ineffective_ballots = '''20
24
26
30
31
33
37
38
39
40'''
