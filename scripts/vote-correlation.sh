#!/bin/bash
# PURPOSE: determine the fraction of contest A, candidate X voters who
#   also voted for choice Y in contest B
#
# EXAMPLE:
#   scripts/vote-correlation.sh -v 1 -d G2018-Pima.db "PROPOSITION 305" "YES" "PROPOSITION 306" "YES"
#


SCRIPT=$(greadlink -f $0)        # Absolute path to this script
SCRIPTPATH=$(dirname $SCRIPT)   # Absolute path this script is in

VERBOSE=0
DB="LVR.db"

usage="USAGE: $cmd [options] race1 choice1 race2 choice
OPTIONS:
  -d <LVR_database>:: LVR DB containing votes (default=$DB)
  -v <verbosity>:: higher number for more output (default=$VERBOSE)
"

while getopts "d:hv:" opt; do
    #! echo "opt=<$opt>"
    case $opt in
        d)
            DB=$OPTARG
            ;;
	h)
            echo "$usage"
            exit 1
            ;;
        v)
            VERBOSE=$OPTARG
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            exit 1
            ;;
        :)
            echo "Option -$OPTARG requires an argument." >&2
            exit 1
            ;;
    esac
done
for (( x=1; x<$OPTIND; x++ )); do shift; done

RAC=1 # Required Argument Count
if [ $# -lt $RAC ]; then
    echo "Not enough non-option arguments. Expect at least $RAC."
    echo >&2 "$usage"
    exit 2
fi
race1=$1
choice1=$2
race2=$3
choice2=$4

##############################################################################

#!title="COUNT of Total votes for ALL Choices, ALL Races"
#!echo "Generating: " $title

# determine the fraction cvr_ids 
#  in vote for        contest A, choice X  
#  also in vote for   contest B, choice Y
###
#
# A CHOICE is already for a specific RACE
#   (multiple CHOICEs can have same TITLE but different RACE_ID).
# SELECT count(vote1), count(vote2, (count(vote1) * 1.0)/count(vote2)
#   WHERE vote1.cvr_id=vote2.cvr_id
#     AND vote1.choice_id=CHOICE1 AND vote2.choice_id=CHOICE2


#sqlite3 -header G2018-Pima.db "SELECT race.title FROM race,choice WHERE race.race_id = choice.race_id AND choice.title=\"$choice1\";"

# echo $race1 $choice1  $race2 $choice2
# PROPOSITION 305 YES/SÍ PROPOSITION 306 YES/SÍ
q1="SELECT choice.choice_id FROM race,choice 
   WHERE race.race_id = choice.race_id 
     AND choice.title LIKE \"${choice1}%\" 
     AND race.title=\"$race1\";"
# => 74
q2="SELECT choice.choice_id FROM race,choice 
   WHERE race.race_id = choice.race_id 
     AND choice.title LIKE \"${choice2}%\" 
     AND race.title=\"$race2\";"
# => 97
dbopts=""
if [ $VERBOSE -gt 0 ]; then
    echo "q1=$q1"
    echo "q2=$q2"
    #dbopts="-echo"
fi
cid1=`sqlite3 $dbopts $DB "$q1"`
cid2=`sqlite3 $dbopts $DB "$q2"`
q3="SELECT race_id FROM race where race.title=\"$race2\";"
rid2=`sqlite3 $dbopts $DB "$q3"`


#!sqlite3 -header G2018-Pima.db "SELECT count(v1.cvr_id),count(v2.cvr_id) FROM vote as v1 INNER JOIN vote as v2 ON v1.choice_id=74 AND v2.choice_id=97 AND v1.cvr_id=v2.cvr_id ;"

# count votes where choice1, choice2 on same ballot
covote=`sqlite3 $DB "SELECT count(v1.cvr_id) FROM vote as v1 INNER JOIN vote as v2 ON v1.choice_id=$cid1 AND v2.choice_id=$cid2 AND v1.cvr_id=v2.cvr_id ;"`


# counts votes for choice1; but only when the ballot contains votes for race2
vote1=`sqlite3 $DB "SELECT count(v1.cvr_id) FROM vote as v1 WHERE v1.choice_id=$cid1 AND EXISTS (SELECT * FROM choice,vote WHERE choice.race_id=$rid2 AND choice.choice_id = vote.choice_id AND vote.cvr_id = v1.cvr_id);"`


if [ $VERBOSE -gt 0 ]; then
    echo "cid1=$cid1"
    echo "cid2=$cid2"
    echo "rid2=$rid2"
    echo "count covote=$covote"
    echo "count vote1=$vote1"
fi

echo "percentage co-vote = $(( 100 * $covote / $vote1 ))%"
