#!/bin/bash -e
# PURPOSE: Output list of Ballot Serial Numbers given list of CVR ids
#
# EXAMPLE:
#

SCRIPT=$(greadlink -f $0)       # Absolute path to this script
SCRIPTPATH=$(dirname $SCRIPT)   # Absolute path this script is in

VERBOSE=0
DB="LVR.db"

usage="USAGE: $cmd [options] cvrid [cvrid...]
OPTIONS:
  -d <LVR_database>:: LVR DB containing votes (default=$DB)
  -v <verbosity>:: higher number for more output (default=$VERBOSE)

EXAMPLES:
  $LVRTOP/scripts/cvr2ser.sh -d MyDatabase.db 
"

while getopts "d:hv:" opt; do
    case $opt in
        d)
            DB=$OPTARG
            ;;
	h)
            echo "$usage"
            exit 1
            ;;
        v)
            VERBOSE=$OPTARG
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            exit 1
            ;;
        :)
            echo "Option -$OPTARG requires an argument." >&2
            exit 1
            ;;
    esac
done
for (( x=1; x<$OPTIND; x++ )); do shift; done

RAC=1 # Required Argument Count
if [ $# -lt $RAC ]; then
    echo "Not enough non-option arguments. Expect at least $RAC."
    echo >&2 "$usage"
    exit 2
fi
outhtml=$1

##############################################################################

title="COUNT of Total votes for ALL Choices, ALL Races"
echo "Generating: " $title

SQL="
SELECT count(vote.cvr_id) as Votes,
       race.title as Race, 
       choice.title as Choice 
FROM vote,choice,race 
WHERE vote.choice_id = choice.choice_id 
  AND choice.race_id = race.race_id 
GROUP BY race.column, choice.choice_id;"
#!sqlite3 -header -column $DB "$SQL"

style=`cat $SCRIPTPATH/style.html`
head="
<!DOCTYPE html>
<html lang=\"en\">
<head>
  <title>$title</title>
$style
</head>

<body>
  <h1>$title</h1>
<table>
"

foot="
</table>
</body>
</html>
"

echo $head > $outhtml
sqlite3 -header -html $DB "$SQL" >> $outhtml
echo $foot >> $outhtml


echo "Missing CVR-IDs:"
sqlite3 $DB  "select cvr_id from cvr order by cvr_id;" \
    | awk '$1!=p+1{print p+1"-"$1-1}{p=$1}'

echo "Wrote: $outhtml"
