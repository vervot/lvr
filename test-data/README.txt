The data in this directory is intended to mimic LVR/CVR data exported from
ES&S.  We try to include all the scenarios that can appear in real LVR
files but with minimal number of rows.

The format of a file is simple.   Its a table in Comma Seperated Value
(CSV) format.  The first N (=3) columns contain metadata about each
Cast Vote Record (CVR) that includes: CVR-id, Precinct, Ballot
Style. All subsequent columns represent Races (Contests). Row one of
the file contains header titles. The remaining rows are data.  If the
header for a Race is blank, it is assumed to be the same value as the
Race header to its left.  This is used to indicate "numberToVoteFor"
Races.  i.e. If there are 3 "Representative" Race columns; the first
will have title, 2 will be blank, taken to mean voters can vote for up
to 3 Candidates.  Each cell in a Race columns represent a choice. A
blank cell under Race means that Race is not associated with the
Ballot for that row (CVR).  In addition to explicit choices from the
ballot (e.g. Candidate name, "Yes", "No") there are three special
choice values.  They are: "undervote", "overvote", Write-in.

For data to be sufficient for testing, it must contain several
features.
1. A "vote for more than one" Race
2. A "vote for one" Race for Office
3. A "vote for one" Race for Proposition
4. Some "vote for more than one" choices that vote for all possible
5. Some "vote for more than one" choices that vote fewer than possible
6. Examples of "undervote"
7. Examples of "overvote"
8. Examples of "Write-in"
9. At least two choices for every Race
