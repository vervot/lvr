* Overview
This software is intended to be used with files exported by ES&S.


* CSV files
**  Via Excel 14.4.0 for Mac 2011
In Excel (for Mac 2011, Version 14.4.0):
“File” menu
	Open file …, browse to desired file and select.  Or file can be opened in Finder with double-click (Excel is the default app for .csv files).
Once file is opened in worksheet:
	Save As …
		Select Format: “Comma Separated Values (.csv)”
		Save As field, enter desired filename
		Save button
